import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State<Assignment4> createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4> {
  bool post1like = false;
  bool post2like = false;
  bool post3like = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
        actions: const [
          Icon(
            Icons.favorite_rounded,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  //color: Colors.amber,
                  child: Image.network(
                    //"https://media.istockphoto.com/id/1307831372/photo/browse-high-resolution-stock-images-of-lord-ganesha.jpg?s=612x612&w=0&k=20&c=ofTjQdTxrp1IR4GI3GFWshS7-mKxrjAZVIDbH9Ts7bI=",
                    //"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1q8EVdsfQXMN8YkOQaCxKZOdAyu5IsmBYrJ-9zNug7-q3Ou8t99a84Ecl-g7e-NUjLVE&usqp=CAU",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIM36vbpCkr_B3ixYq3MEEv2B2qaqR2SKnOw&usqp=CAU",
                    width: double.infinity,
                    //  height: 300,
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          post1like = !post1like;
                        });
                      },
                      icon: post1like
                          ? const Icon(
                              Icons.favorite_rounded,
                              color: Colors.orange,
                            )
                          : const Icon(
                              Icons.favorite_outline_outlined,
                            ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.comment_outlined),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.send,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.bookmark_outline,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  //color: Colors.amber,
                  child: Image.network(
                    //"https://media.istockphoto.com/id/1307831372/photo/browse-high-resolution-stock-images-of-lord-ganesha.jpg?s=612x612&w=0&k=20&c=ofTjQdTxrp1IR4GI3GFWshS7-mKxrjAZVIDbH9Ts7bI=",
                    //"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1q8EVdsfQXMN8YkOQaCxKZOdAyu5IsmBYrJ-9zNug7-q3Ou8t99a84Ecl-g7e-NUjLVE&usqp=CAU",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIM36vbpCkr_B3ixYq3MEEv2B2qaqR2SKnOw&usqp=CAU",
                    width: double.infinity,
                    // height: 300,
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          post2like = !post2like;
                        });
                      },
                      icon: Icon(
                        post2like
                            ? Icons.favorite_rounded
                            : Icons.favorite_outline_outlined,
                        color: post2like ? Colors.red : Colors.black,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.comment_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.send,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.bookmark_outline,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  //color: Colors.amber,
                  child: Image.network(
                    //"https://media.istockphoto.com/id/1307831372/photo/browse-high-resolution-stock-images-of-lord-ganesha.jpg?s=612x612&w=0&k=20&c=ofTjQdTxrp1IR4GI3GFWshS7-mKxrjAZVIDbH9Ts7bI=",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1q8EVdsfQXMN8YkOQaCxKZOdAyu5IsmBYrJ-9zNug7-q3Ou8t99a84Ecl-g7e-NUjLVE&usqp=CAU",
                    width: double.infinity,
                    height: 200,
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          post3like = !post3like;
                        });
                      },
                      icon: Icon(
                        post3like
                            ? Icons.favorite_rounded
                            : Icons.favorite_outline_outlined,
                        color: post3like ? Colors.red : Colors.black,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.comment_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.send,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.bookmark_outline,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
