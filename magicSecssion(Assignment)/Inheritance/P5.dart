class Test {
  int x = 10;
  String str = "Core2Web";

  void parentMethod() {
    print(str);
    print(x);
  }
}

class Test2 extends Test {
  int x = 10;
  String str = "Incubator";

  void childMethod() {
    print(x);
    print(str);
  }
}

void main() {
  Test2 obj = new Test2();

  obj.parentMethod();
  obj.childMethod();
}
