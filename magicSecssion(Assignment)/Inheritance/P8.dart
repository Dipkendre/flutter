class Parent {
  Parent() {
    print("In P Constructor");
  }
}

class Child extends Parent {
  Child() {
    super();
    print("In C Constructor");
  }
}

void main() {
  Child obj = new Child();
}
